package com.example.demo.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.example.model.Country;
import com.example.service.CountryService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc 
public class CountryControllerTest {
	
	@MockBean
	CountryService countryService;
	
	@Autowired
	MockMvc mockMvc;
	
	
	@Test
	public void testFindAllCountries() throws Exception {
		List<Country> countries = new ArrayList<Country>() {{
			add(new Country(1, "Morocco"));
			add(new Country(2, "Palestine"));
		}};
		
		Mockito.when(countryService.getCountries()).thenReturn(countries);
		// System.out.println("Countries ----> " + countries.size());
		assertEquals(countries.size(), 2);
		mockMvc.perform(get("/countries/"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", Matchers.hasSize(0)));
	}
}
