package com.example.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.model.City;
import com.example.repository.CityRepository;

@Service
public class CityService {
	
	private final CityRepository cityRepository;
	
	public CityService(CityRepository cityRepository) {
		this.cityRepository = cityRepository;
	}

	public List<City> getCities() {
		return this.cityRepository.findAll();
	}
	
	public Optional<City> getCity(int id) {
		return this.cityRepository.findById(id);
	}
	
	public City add(City city) {
		return this.cityRepository.save(city);
	}
	
	public List<City> getCitiesStartsWith(String city) {
		return this.cityRepository.findByCityStartsWith(city);
	}
}
