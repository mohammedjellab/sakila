package com.example.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.model.Country;
import com.example.repository.CountryRepository;

@Service
public class CountryService {

	private final CountryRepository countryRepository;
	
	public CountryService(CountryRepository countryRepository) {
		this.countryRepository = countryRepository;
	}
	
	public List<Country> getCountries() {
		return this.countryRepository.findAll();
	}
	
	public Optional<Country> getCountry(int id) {
		return this.countryRepository.findById(id);
	}
	
	public Country add(Country country) {
		return this.countryRepository.save(country);
	}
	
	public List<Country> countries() {
		return this.countryRepository.findCountries();
	}
	
	public int countCountries() {
		return this.countryRepository.countCountries();
	}
}
