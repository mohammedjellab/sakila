package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.model.Country;


@Repository
public interface CountryRepository extends JpaRepository<Country, Integer>{
	@Query("SELECT c FROM Country c")
	List<Country> findCountries();
	
	@Query(value="SELECT COUNT(*) FROM Country", nativeQuery=true)
	int countCountries();
}
