package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.model.Actor;

@Repository
public interface ActorRepository extends JpaRepository<Actor, Integer> {

}
