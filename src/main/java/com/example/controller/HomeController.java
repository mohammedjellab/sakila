package com.example.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Country;
import com.example.service.CountryService;

@RestController
@RequestMapping("countries")
public class HomeController {

	private final CountryService countryService;
	
	public HomeController(CountryService countryService) {
		this.countryService = countryService;
	}
	
	@GetMapping("/")
	public List<Country> index() {
		return this.countryService.countries();
	}
	
	@GetMapping("/count")
	public int count() {
		return this.countryService.countCountries();
	}
}
