package com.example.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.City;
import com.example.service.CityService;

@RestController
@RequestMapping("cities")
public class CityController {

	private final CityService cityService;

	public CityController(CityService cityService) {
		this.cityService = cityService;
	}

	@GetMapping("/")
	public List<City> index() {
		return this.cityService.getCities();
	}

	@GetMapping("/{city}")
	public List<City> getCitiesByCity(@PathVariable String city) {
		return this.cityService.getCitiesStartsWith(city);
	}
}
