# War config with tomcat
# FROM tomcat:8.5.79-jre8-openjdk-slim-buster
# ADD target/demo.war /usr/local/tomcat/webapps
# EXPOSE 8080
# CMD ["catalina.sh", "run"]


# Jar config
# FROM openjdk:8-jdk-alpine
# ARG JAR_FILE=target/*.jar
# COPY ${JAR_FILE} demo.jar
# ENTRYPOINT ["java", "-jar", "/demo.jar"]

FROM openjdk:8-jdk-alpine
ADD target/demo-0.0.1-SNAPSHOT.jar demo-0.0.1-SNAPSHOT.jar
# EXPOSE 9000
ENTRYPOINT ["java", "-jar", "demo-0.0.1-SNAPSHOT.jar"]